#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

const int dim [15] = {127,17,29,59,113,227,449,907,1801,3593,7177,14341,28687,57347,114689};
int size; // dim[size] will be the current size of the table

typedef struct list
{
	char val[30];
	int count;
	struct list *next;
}List;

typedef struct hash{
	List **table;
}hash_table;

int hash1(char x[])// we will convert any words in lowcase :D
{
	int i, hash=0;
	for(i=0;x[i];i++)
		x[i] = tolower(x[i]);
	for(i=0;x[i];i++)
		hash += x[i];
	return hash;
}

hash_table *create_hash()
{
	int i;
	hash_table *new_hash;
	new_hash = calloc(1,sizeof(hash_table)); //allocate memory for the table structure
	new_hash->table = calloc(dim[size],sizeof(List*)); //allocate memory for the table itself

	for(i=0;i<dim[size];i++)
		new_hash->table[i] = NULL;

	return new_hash;
}

int isletter(char a)
{
	if( (a >= 'a') && (a <= 'z'))
		return 1;
	if( (a >= 'A') && (a <= 'Z'))
		return 1;
	if( (a >= '0') && (a <= '9'))
		return 1;
	return 0;
}

int signs(char x[]) //checks if the word I currently try to add contains any signs. For example, dog-in-the-blanket
{
	if( strchr(x,'-') || strchr(x,':') || strchr(x,'?') || strchr(x,'.') || strchr(x,',') || strchr(x,'!') || strchr(x,'(') || strchr(x,')') || strchr(x,';'))
	{
		return 1;
	}
	return 0;
}

void cpy(char a[], char b[]) //will copy b in a
{
	int i=0;
	while( b[i] )
	{
		a[i] = b[i];
		i++;
	}
	a[i++] = '\0';
}

List  *lookup(hash_table *hash, char x[])
{
	List *list;
	int hashval = hash1(x) % dim[size];

	for(list = hash->table[hashval]; list != NULL;list=list->next)
	{
		if(strcasecmp(x,list->val) == 0)
			return list;
	}
	return NULL;
}

int full(hash_table *a) //Will return 0 if table is not 75% full
{	
	int i=0,count=0;
	for(i=0;i<=dim[size];i++)
	{
		if(a->table[i] != NULL)
			count++;
	}
	double x ;
	x = (double)(count / dim[size]);
	if( x > 0.75)
		return 1;
	return 0;
}

void addhash( hash_table *hash, char x[])
{
	int found = 0; // IF the item is found in the list, i will make it 1
	List *new_list = calloc(1,sizeof(List));
	List *current_list;
	int hashval = hash1(x) % dim[size]; // the key in the table;
	current_list = lookup(hash, x);
	if(current_list != NULL)
	{
		found = 1;
		current_list->count += 1;
	}
	if(found == 0)
	{
		cpy(new_list->val,x);
		new_list->next = hash->table[hashval];
		hash->table[hashval] = new_list;
	}
}

void read_word(FILE *f, char x[])
{
	int i=0;
	while(!feof(f))
	{
		fscanf(f,"%c",&x[i]);
		if(strchr(" ,.?!:;()*\n-\0",x[i]))
		{
			x[i] = '\0';
			break;
		}
		i++;
	}
}

void read_hash(FILE *f, hash_table *a)
{
	char x[30];
	int ki = 0;
	int len = strlen(x) -1;
	int ok=0;
	while(!feof(f))
	{
		int i;
		char aux[30];
		read_word(f,x);
		if(strlen(x) && x[0] != '\0')
			addhash(a,x);
	}
			
}

void found (hash_table *hash, char x[], int *count, int *app) // -1 means that the word is not there
{
	*count = 0;
	List *lista;
	lista = lookup(hash,x);
	if(lista == NULL)
	{
		*app = -1;
		return;
	}
	while(lista ->next != NULL)
	{
		if(strcasecmp(lista->val,x) == 0)
		{
			*app = lista->count;
			return;
		}
		*count = *count + 1;
		lista = lista -> next;
	}
	if( (lista->next == NULL) && (strcasecmp(lista->val,x) == 0) )
		{
			*app = lista->count;
			return;
		}
		*app = -1;
}

void printList(hash_table *myTable, int key, FILE *g)
{
	List *l;// = calloc(1, sizeof(list));
	for(l = myTable->table[key]; l!=NULL; l=l->next)
	{
		fprintf(g,"%s %d ", l->val, l->count);	
		fprintf(g, "\n");
	}
	fprintf(g, "----------------------------\n");
}
void printTable(hash_table *myTable)
{
	int i;
	FILE *g = fopen("write2.txt", "w");
	//printf("%d\n",size );
	for(i=0; i<dim[size]; i++)
	{
	printList(myTable, i, g);
	fprintf(g, "%d +++++++++++++++++++++\n", i);;
	}
	fclose(g);
}

void query(hash_table *hash)
{
	char aux; //Apparently there is a \n that made my tests crash, this is just a dummy variable 
	char auxy; // y/n
	char x[30]; // the word 
	
	printf("Enter word for retrieval: ");
	scanf("%s",x);
	
	
	int count=0,app=0;
	found(hash, x, &count, &app);
	if(app == -1 )
		printf("The word was not found!\n");
	else
	{
		printf("'%s' occurs %d times.\n",x,app+1);
		printf("Retrieval from list took %d comparisons\n",count+1);
	}
	printf("Another query? [y/n] ");
	scanf("%c",&aux);
	scanf("%c",&auxy);
	printf("\n");
	
	while(auxy == 'y')
	{
	printf("Enter word for retrieval: ");
	scanf("%s",x);
	count=0,app=0;
	found(hash, x, &count, &app);
	if(app == -1)
		printf("The word was not found!\n");
	else
	{
		printf("'%s' occurs %d times.\n",x,app+1);
		printf("Retrieval from list took %d comparisons\n",count+1);
	}
	printf("Another query? [y/n] ");
	scanf("%c",&aux);
	scanf("%c",&auxy);
	printf("\n");
	}
}

int main(int argc, char *argv[])
{
	clock_t t;
	t=clock();
	
	FILE *filePointer = fopen(argv[1],"r"); //file points to a_portrait.txt and returns NULL if the file doesn't exist
	if(filePointer == NULL) //failure to open file
	{
		printf("ERROR: Cannot open file.\n");
		return 1; // Exit function with failure
	}
	else
	{
		hash_table *hash;
		hash = create_hash(dim[size]);
		read_hash(filePointer,hash);
		printf("%d \n",full(hash));
		//query(hash);
		printTable(hash);
		return 0;
	}
}